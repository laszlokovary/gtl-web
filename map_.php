<?php
// Get parameters from URL
$phoneid = $_GET["phoneid"];
$postime = $_GET["postime"];
if (!$phoneid){
	die("Invalid parameter(s)!");
}

// Opens a connection to a mySQL server
$connection = mysqli_connect("localhost","dbeklsofttrade","dbekls0fttrade","eklgtl");
if (!$connection) {
	die("Not connected : " . mysql_error());
}
if (mysqli_connect_errno()) {
	die ("Failed to connect to MySQL: " . mysqli_connect_error());
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>

function initialize() {

  // mapOptions
  var mapOptions = {
    zoom: 10,

  <?php

    $did = mysqli_real_escape_string($connection, $phoneid);

    $qry = '';
    if (!$postime) {
	// Search the rows in the markers table
	$qry = "SELECT lat, lng FROM markers WHERE phoneid = '$did' ORDER BY id limit 1 ";
    }
    else {
	$dt = mysqli_real_escape_string($connection, $postime);
	$qry = "SELECT lat, lng FROM markers WHERE (phoneid = '$did') AND (INSTR(`postime`, '$dt') > 0) ORDER BY id limit 1 ";
    }
    $query = $qry;


    $result = mysqli_query($connection, $query);
    // Iterate through the rows, adding XML nodes for each
    while ($row = mysqli_fetch_array($result)) {
	echo "center: new google.maps.LatLng({$row['lat']}, {$row['lng']}),";
    }
  ?>    
    mapTypeId: google.maps.MapTypeId.ROADMAP
 }; // mapOptions 

 var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

 var routePoints = [

 <?php

  $did = mysqli_real_escape_string($connection, $phoneid);

   $qry = '';
   if (!$postime){
	// Search the rows in the markers table
	$qry = "SELECT * FROM markers WHERE phoneid = '$did' ORDER BY id ";
   }
   else {
	$dt = mysqli_real_escape_string($connection, $postime);
	$qry = "SELECT * FROM markers WHERE (phoneid = '$did') AND (INSTR(`postime`, '$dt') > 0) ORDER BY id ";
   }
   $query = $qry;

   $result = mysqli_query($connection, $query);
   // Iterate through the rows, adding XML nodes for each
   while ($row = mysqli_fetch_array($result)) {
	echo "new google.maps.LatLng({$row['lat']}, {$row['lng']}),";
   }
 ?>

 ];
  
 var routePath = new google.maps.Polyline({
   path: routePoints,
   geodesic: true,
   strokeColor: '#FF0000',
   strokeOpacity: 1.0,
   strokeWeight: 3
 });

 routePath.setMap(map);

 function addInfoWindow(marker, message) {

        var boxText = document.createElement("div");
        boxText.style.cssText = "border: 2px solid blue; margin-top: 5px; background: yellow; padding: 5px;";
        boxText.innerHTML = message;
		
	var myOptions = {
		content: boxText,
		disableAutoPan: false,
		maxWidth: 200,
		pixelOffset: new google.maps.Size(-140, 50),
		zIndex: null,
		boxStyle: { 
		  opacity: 0.75,
		  width: "580px",
		  background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat"
		 },
		closeBoxMargin: "20px 20px 20px 20px",
		closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
		infoBoxClearance: new google.maps.Size(1, 1),
		isHidden: false,
		pane: "floatPane",            
		enableEventPropagation: false
	};

   var infowindow = new google.maps.InfoWindow(myOptions);



   google.maps.event.addListener(marker, 'click', function() {
     infowindow.open(marker.get('map'), marker);
   }); 
 }

 var routePointsTitles = [  

 <?php
   $did = mysqli_real_escape_string($connection, $phoneid);

   $qry = '';
   if (!$postime){
	// Search the rows in the markers table
	$qry = "SELECT * FROM markers WHERE phoneid = '$did' ORDER BY id ";
   }
   else {
	$dt = mysqli_real_escape_string($connection, $postime);
	$qry = "SELECT * FROM markers WHERE (phoneid = '$did') AND (INSTR(`postime`, '$dt') > 0) ORDER BY id ";
   }
   $query = $qry;

   $result = mysqli_query($connection, $query);
   // Iterate through the rows, adding XML nodes for each
   while ($row = mysqli_fetch_array($result)) {
	echo "'".$row['postime']."',";
    }
  
 ?>
 ];

 var image = 'placemark_icon_blue_flag_16x16.png';


 var len = routePointsTitles.length;
 for (var i = 0; i<len; i++) {

   var ix = i + 1;
   var titleTm =  ix + '. ' + routePointsTitles[i];
   var subTitleTm =  routePointsTitles[i];
   var loc = routePoints[i];

   var infoWindowContent = "<b> " + ix + ". Time " + subTitleTm + "</b><br>Lat " + loc.lat() + "<br>Lng " + loc.lng() + "<br>";


   var marker = new google.maps.Marker({
      position: loc,
      map: map,
      icon: image,
      animation: google.maps.Animation.DROP,
      title: titleTm
   });

   addInfoWindow(marker, infoWindowContent);

   marker.setMap(map);
 }



 <?php 
   mysqli_close($connection); 
 ?>
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
    <div id="map-canvas"></div>
  </body>
</html>